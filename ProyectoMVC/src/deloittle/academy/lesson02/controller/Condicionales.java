package deloittle.academy.lesson02.controller;

import java.util.logging.Logger;

/**
 * Clase de condicionales
 * 
 * @author dmascott@externosdeloittemx.com
 * @since 03/10/2020
 * @version 1.0
 *
 */
public class Condicionales {

	/**
	 * Solo es para mostrar que funcione, una vez probado se iba a borrar
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(mayorQue(15.00, 13.30));
		System.out.println(diferentes("Hola Mundo Java", "Hola Mundo"));
		System.out.println(colorRojo("Rojo"));
		System.out.println(asientos("C3"));
		System.out.println(sorpresa(6));
	}

	/*
	 * Se declara una variable logger global
	 */
	public final static Logger LOGGER = Logger.getLogger(Condicionales.class.getName());

	/**
	 * M�todo mayorQue
	 * 
	 * @param valor1 Valor double
	 * @param valor2 Valor double
	 * @return Valor double
	 * 
	 *         Verifica cual es el valor mayor de ambos enviados en los par�metros y
	 *         regresa el mayor
	 */
	public static double mayorQue(double valor1, double valor2) {
		double mayor = (valor1 > valor2) ? valor1 : valor2;
		return mayor;
	}

	/**
	 * M�todo diferentes
	 * 
	 * @param cadena1 Valor String
	 * @param cadena2 Valor String
	 * @return Valor String
	 * 
	 *         Verifica si las cadenas son iguales o no y regresa un mensaje
	 */
	public static String diferentes(String cadena1, String cadena2) {
		String tamano = (cadena1.length() != cadena2.length()) ? "Las cadenas son diferentes"
				: "Las cadenas son iguales";
		return tamano;
	}

	/**
	 * M�todo colorRojo
	 * 
	 * @param rojo Valor String
	 * @return Valor String
	 * 
	 *         Verifica si el valor regresado es igual a Rojo, envia mensaje de
	 *         error o verifica
	 */
	public static String colorRojo(String rojo) {
		String color = (rojo == "Rojo") ? "Est�s en lo correcto" : "Te has equivocado";
		return color;
	}

	/**
	 * M�todo asientos
	 * 
	 * @param codigoAsiento Valor String
	 * @return Valor String
	 * 
	 *         Recibe un valor y si el valor es C3 lo manda como ocupada cualquier
	 *         otro valor lo manda como desocupado
	 */
	public static String asientos(String codigoAsiento) {
		String vacio;

		if (codigoAsiento == "C3") {
			vacio = "El asiento est� ocupado";
		} else {
			vacio = "El asiento est� desocupado";
		}

		return vacio;
	}

	/**
	 * M�todo sorpresa
	 * 
	 * @param numero Valor int
	 * @return Valor String
	 * 
	 *         Dependiendo del n�mero ingresado env�a un mensaje
	 */
	public static String sorpresa(int numero) {
		String premio;

		if (numero == 0) {
			premio = "Has ganado un auto";
		} else if (numero == 1) {
			premio = "Has ganado una casa";
		} else if (numero == 2) {
			premio = "Has ganado un viaje";
		} else {
			premio = "No has ganado nada, sigue participando";
		}

		return premio;
	}
}

package deloittle.academy.lesson02.controller;

import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.logging.Logger;

import deloittle.academy.lesson02.model.Peliculas;

/**
 * Controlador Peliculas
 * 
 * @author dmascott@externosdeloittemx.com
 * @since 03/10/2020
 * @version 1.0
 *
 */
public class ControllerPeliculas {

	/*
	 * Se definen tres variables globales ListaPeliculas que contendr� valores del
	 * objeto Peliculas pruebaPelicula que contendr� un valor objeto Peliculas y un
	 * Logger
	 */
	public static final ArrayList<Peliculas> listaPeliculas = new ArrayList<Peliculas>();
	public static Peliculas pruebaPelicula = new Peliculas();
	public final static Logger LOGGER = Logger.getLogger(ControllerPeliculas.class.getName());

	/**
	 * Solo es para mostrar que funcione, una vez probado se iba a borrar
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Peliculas peli1 = new Peliculas();
		Peliculas peli2 = new Peliculas();
		peli1.setId(1);
		peli1.setNombre("Shrek");
		peli1.setDescripcion("Pelicula muy cool");
		peli1.setIdioma("Espa�ol Latinoamerica");
		peli1.setCategoria("Infantil");
		peli1.setDuracion(1.30);
		peli1.setClasificacion("A");
		peli1.setCodCategoria(1);
		peli1.setActrizPrincipal("Cameron Diaz");
		peli1.setActorPrincipal("Mike Myers");
		peli1.setEscenasPostcreditos(false);
		peli1.setSubtitulada(false);
		peli1.setCantidadSalas(5);
		peli1.setAtributo1(0);
		peli1.setAtributo2(null);
		peli1.setAtributo3(null);
		peli1.setAtributo4(0);
		peli1.setAtributo5(0);
		peli1.setAtributo6(false);
		peli1.setAtributo7(null);

		peli2.setId(2);
		peli2.setNombre("Los juegos del hambre");
		peli2.setDescripcion("Pelicula medio cool");
		peli2.setIdioma("Ingles");
		peli2.setCategoria("Adolescentes");
		peli2.setDuracion(1.50);
		peli2.setClasificacion("B");
		peli2.setCodCategoria(2);
		peli2.setActrizPrincipal("Jennifer Lawrence");
		peli2.setActorPrincipal("Josh Hutcherson");
		peli2.setEscenasPostcreditos(false);
		peli2.setSubtitulada(true);
		peli2.setCantidadSalas(4);
		peli2.setAtributo1(0);
		peli2.setAtributo2(null);
		peli2.setAtributo3(null);
		peli2.setAtributo4(0);
		peli2.setAtributo5(0);
		peli2.setAtributo6(false);
		peli2.setAtributo7(null);

		agregarPelicula(peli1);
		agregarPelicula(peli2);
		System.out.println(listaPeliculas.get(0).getNombre());
		System.out.println(buscarPorNombre("Los juegos del hambre").getId());
		System.out.println(buscarTodo());

	}

	/**
	 * M�todo agregarPeliculas
	 * 
	 * @param peliculas Objeto Peliculas
	 * 
	 *                  Agrega el objeto recibido a la lista de peliculas
	 */
	public static void agregarPelicula(Peliculas peliculas) {
		try {
			listaPeliculas.add(peliculas);
			LOGGER.info("Se a�adio correctamente el valor del Atributo2");
		} catch (Exception e) {
			LOGGER.info("Error al agregar el valor");
		}
	}

	/**
	 * M�todo agregarAtributo2
	 * 
	 * @param pelicula  Objeto Peliculas
	 * @param atributo2 Valor String
	 * 
	 *                  Agrega el valor enviado al atributo2
	 */
	public static void agregarAtributo2(Peliculas pelicula, String atributo2) {
		try {
			pelicula.setAtributo2(atributo2);
			LOGGER.info("Se a�adio correctamente el valor");
		} catch (Exception e) {
			LOGGER.info("Error al agregar el valor");
		}
	}

	/**
	 * M�todo buscarPorNombre
	 * 
	 * @param nombre Valor String
	 * @return Objeto Peliculas
	 * 
	 *         Ubica cual objeto en la listaPeliculas coincide con el nombre enviado
	 *         en los parametros
	 */
	public static Peliculas buscarPorNombre(String nombre) {
		try {
			listaPeliculas.forEach(new Consumer<Peliculas>() {
				@Override
				public void accept(Peliculas pelicula) {
					if (pelicula.getNombre() == nombre) {
						pruebaPelicula = pelicula;
					}
				}
			});
			LOGGER.info("Se realiz� con �xito la b�squeda");
		} catch (Exception e) {
			LOGGER.info("Error al realizar la b�squeda");
		}
		return pruebaPelicula;
	}

	/**
	 * M�todo buscarTodo
	 * 
	 * @return ArrayList con valores String
	 * 
	 *         Agrega en el array los valores de cada pelicula en la lista
	 */
	public static ArrayList<String> buscarTodo() {
		ArrayList<String> pelicula = new ArrayList<String>();
		try {
			listaPeliculas.forEach(pelicula1 -> pelicula.add("Nombre: " + pelicula1.getNombre() + "\n" + "Descripci�n: "
					+ pelicula1.getDescripcion() + "\n" + "Idioma: " + pelicula1.getIdioma() + "\n" + "Categor�a: "
					+ pelicula1.getCategoria() + "\n" + "Duraci�n: " + pelicula1.getDuracion() + "\n"
					+ "Clasificaci�n: " + pelicula1.getClasificacion() + "\n"));
			LOGGER.info("Se realiz� con �xito la b�squeda");
		} catch (Exception e) {
			LOGGER.info("Error al realizar la b�squeda");
		}
		return pelicula;
	}
}

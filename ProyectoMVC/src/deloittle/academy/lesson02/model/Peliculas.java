package deloittle.academy.lesson02.model;

/**
 * Clase entidad Peliculas
 * 
 * @author dmascott@externosdeloittemx.com
 * @since 03/10/2020
 * @version 1.0
 *
 */
public class Peliculas {

	private int id;
	private String nombre;
	private String descripcion;
	private String idioma;
	private String categoria;
	private double duracion;
	private String clasificacion;
	private int codCategoria;
	private String actrizPrincipal;
	private String actorPrincipal;
	private boolean escenasPostcreditos;
	private boolean subtitulada;
	private int cantidadSalas;
	private int atributo1;
	private String atributo2;
	private String atributo3;
	private int atributo4;
	private double atributo5;
	private boolean atributo6;
	private String atributo7;

	/*
	 * Constructor de Superclase
	 */
	public Peliculas() {
		super();
	}

	/**
	 * Constructor con todos los par�metros
	 * 
	 * @param id                  Valor int
	 * @param nombre              Valor String
	 * @param descripcion         Valor String
	 * @param idioma              Valor String
	 * @param categoria           Valor String
	 * @param duracion            Valor double
	 * @param clasificacion       Valor String
	 * @param codCategoria        Valor int
	 * @param actrizPrincipal     Valor String
	 * @param actorPrincipal      Valor String
	 * @param escenasPostcreditos Valor boolean
	 * @param subtitulada         Valor boolean
	 * @param cantidadSalas       Valor int
	 * @param atributo1           Valor int
	 * @param atributo2           Valor String
	 * @param atributo3           Valor String
	 * @param atributo4           Valor int
	 * @param atributo5           Valor double
	 * @param atributo6           Valor boolean
	 * @param atributo7           Valor String
	 */
	public Peliculas(int id, String nombre, String descripcion, String idioma, String categoria, double duracion,
			String clasificacion, int codCategoria, String actrizPrincipal, String actorPrincipal,
			boolean escenasPostcreditos, boolean subtitulada, int cantidadSalas, int atributo1, String atributo2,
			String atributo3, int atributo4, double atributo5, boolean atributo6, String atributo7) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.idioma = idioma;
		this.categoria = categoria;
		this.duracion = duracion;
		this.clasificacion = clasificacion;
		this.codCategoria = codCategoria;
		this.actrizPrincipal = actrizPrincipal;
		this.actorPrincipal = actorPrincipal;
		this.escenasPostcreditos = escenasPostcreditos;
		this.subtitulada = subtitulada;
		this.cantidadSalas = cantidadSalas;
		this.atributo1 = atributo1;
		this.atributo2 = atributo2;
		this.atributo3 = atributo3;
		this.atributo4 = atributo4;
		this.atributo5 = atributo5;
		this.atributo6 = atributo6;
		this.atributo7 = atributo7;
	}

	/*
	 * Creaci�n de getters y setters
	 */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public double getDuracion() {
		return duracion;
	}

	public void setDuracion(double duracion) {
		this.duracion = duracion;
	}

	public String getClasificacion() {
		return clasificacion;
	}

	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}

	public int getCodCategoria() {
		return codCategoria;
	}

	public void setCodCategoria(int codCategoria) {
		this.codCategoria = codCategoria;
	}

	public String getActrizPrincipal() {
		return actrizPrincipal;
	}

	public void setActrizPrincipal(String actrizPrincipal) {
		this.actrizPrincipal = actrizPrincipal;
	}

	public String getActorPrincipal() {
		return actorPrincipal;
	}

	public void setActorPrincipal(String actorPrincipal) {
		this.actorPrincipal = actorPrincipal;
	}

	public boolean isEscenasPostcreditos() {
		return escenasPostcreditos;
	}

	public void setEscenasPostcreditos(boolean escenasPostcreditos) {
		this.escenasPostcreditos = escenasPostcreditos;
	}

	public boolean isSubtitulada() {
		return subtitulada;
	}

	public void setSubtitulada(boolean subtitulada) {
		this.subtitulada = subtitulada;
	}

	public int getCantidadSalas() {
		return cantidadSalas;
	}

	public void setCantidadSalas(int cantidadSalas) {
		this.cantidadSalas = cantidadSalas;
	}

	public int getAtributo1() {
		return atributo1;
	}

	public void setAtributo1(int atributo1) {
		this.atributo1 = atributo1;
	}

	public String getAtributo2() {
		return atributo2;
	}

	public void setAtributo2(String atributo2) {
		this.atributo2 = atributo2;
	}

	public String getAtributo3() {
		return atributo3;
	}

	public void setAtributo3(String atributo3) {
		this.atributo3 = atributo3;
	}

	public int getAtributo4() {
		return atributo4;
	}

	public void setAtributo4(int atributo4) {
		this.atributo4 = atributo4;
	}

	public double getAtributo5() {
		return atributo5;
	}

	public void setAtributo5(double atributo5) {
		this.atributo5 = atributo5;
	}

	public boolean isAtributo6() {
		return atributo6;
	}

	public void setAtributo6(boolean atributo6) {
		this.atributo6 = atributo6;
	}

	public String getAtributo7() {
		return atributo7;
	}

	public void setAtributo7(String atributo7) {
		this.atributo7 = atributo7;
	}

}
